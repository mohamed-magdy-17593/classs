import {composeClasssL, composeClasssR, composeClasss} from '../index'

const composeClasssProps = composeClasss({aa: 'aa', bb: 'bb'})

test('composeClasss behavior', () => {
  expect(typeof composeClasssProps).toBe('function')
  expect(composeClasssProps(['a', 'b'])).toEqual({className: 'a b'})
})

test('deep composeClasss behavior', () => {
  expect(
    composeClasssProps([
      'a',
      'b',
      ['c', [[undefined]], {color: 'green'}, ({aa, bb}) => [aa, bb]],
    ]),
  ).toEqual({
    className: 'a b c aa bb',
    style: {color: 'green'},
  })
})

test('composeClasssL testing with props', () => {
  expect(
    composeClasssL({
      a: 'a',
      className: 'aa bb',
      style: {color: 'black'},
      classs: ['g', ['f'], {color: 'green', font: 'happy'}],
    }),
  ).toEqual({
    a: 'a',
    className: 'aa bb g f',
    style: {color: 'green', font: 'happy'},
  })
})

test('composeClasssR testing with props', () => {
  expect(
    composeClasssR({
      a: 'a',
      className: 'aa bb',
      style: {color: 'black'},
      classs: ['g', ['f'], {color: 'red', font: 'happy'}],
    }),
  ).toEqual({
    a: 'a',
    className: 'g f aa bb',
    style: {color: 'black', font: 'happy'},
  })
})
