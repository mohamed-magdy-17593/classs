import React from 'react'
import {
  mergeWith,
  cond,
  isString,
  addStringWith,
  isObject,
  merge,
  emptyObjectAssing,
  isFunction,
  isArray,
  reduce,
  T,
} from './utils'

const Sconcat = mergeWith(
  cond([[isString, addStringWith(' ')], [isObject, merge]]),
)

const composeClasss = props => {
  const composeFn = cond([
    [isString, emptyObjectAssing('className')],
    [isObject, emptyObjectAssing('style')],
    [isFunction, f => composeFn(f(props))],
    [isArray, reduce((acc, a) => Sconcat(acc, composeFn(a)))({})],
    [T, {}],
  ])
  return composeFn
}

const composeClasssFactory = composeOrderFn => ({
  classs,
  className,
  style,
  ...restProps
}) => ({
  ...restProps,
  ...composeClasss(restProps)(composeOrderFn(className, style, classs)),
})
const composeClasssL = composeClasssFactory((className, style, classs) => [
  className,
  style,
  classs,
])
const composeClasssR = composeClasssFactory((className, style, classs) => [
  classs,
  className,
  style,
])

const classs = (type, props, ...children) =>
  React.createElement(type, composeClasssL(props), ...children)

export {composeClasssFactory, composeClasssL, composeClasssR, composeClasss}
export default classs
