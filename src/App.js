/* @jsx classs */
import classs from './classs' // eslint-disable-line
import React from 'react'
import styless from './classs/styless'

const cool = ['ahmed', {background: 'red'}]

const H1 = styless('h1')([
  cool,
  {fontSize: 80},
  'elsayed',
  'nega',
  ({outline}) => outline && {border: '1px solid black'},
  ({ali = 'black'}) => ({
    color: ali,
  }),
])

function App() {
  return (
    <div>
      <H1 classs={['b', 'h']} ali="green">
        Hola
      </H1>
    </div>
  )
}

export default App
